The purpose of this project is to enable the classification of Terror text that is written by ISIS members.
In order to prove the concept of such a classification we received two labeled lists with textual tweets. 
the two lists were labeled as Terror and non-Terror, '1' and '0' respectively.
the major difference between the two lists is their length, which is '10' to '1' between non-Terror to Terror.
the relation between the two groups will need our attention in the solution, in order to maximize our results.

our solution, like every statistical and ML solution, is divided as following:

1) loading the data

2) cleaning the data

3) standardization of the input data

4) splitting the data into train and test

5) training the data and testing it on un-known data

6) summarization of our results

7) conclusions 

Solution: 

the solution is given in the attached python file 'ISISORNOT_PIPE'.
we read the two excel files, and remove the lines that are not following the standard format.
In order to put the sentences in contextual format we remove all single characters signs, and spaces.
Then, we vectorize our sentences with bag of words, where the bag includes 1500 words.
Moreover, we find the frequency of each word in the sentence itself and the frequency in all the data, in percentage.
The two last steps are saved as *.joblib file.

Now, we randomly split the data into train and test, where the relations are '0.8' and '0.2' respectively.
In order to make sure that the results are not biased we run the training 10 times with random splitting of the data into train and test.
due to the fact that we deal with a binary problem, we choose to train with Logistic Regression classifier.

Results:

at first, we run the classifier without any reference to the partition of the data ('10' to '1').
the score for this execution was ~ 97%, however the confusion matrix presented an un-balance accuracy between Terror to Non-Terror tweets, as can be seen in the figure [ISISORNOT_NO_WEIGHTS.png].
However, when we change the weights of each group the classifier, we could get a bit less in the score, ~95%, but with balanced confusion matrix [ISISORNOT.png].
we believe, that the second result is preferred, because the change in the score is minimal in comparison to the change in the balanced results.
we also found that the standard deviation of the accuracy is less than 1%, which points on robust and unbiased classifier.
In Both cases, the difference in accuracy between training and testing was negligible. 

In order to classify new data, we will load the data, clean and formatting it, and then use the pipeline file '.joblib' in order to vectorize it.
the vectorized data will be the input to the classifier.

It is valuable to examine the data by displaying its spread on 2D space. the straightforward way to do this, is by using its principal components given by PCA.
as one can see in figure [PCA_ANAL.png] the ISIS data is localized in specific area in the 2D plot (red dots) while other data is spread all over (blue dots).
it means that the odds for success in this problem are higher than the odds for failure.

In conclusion, we believe that it is possible to achieve even more accurate and balanced results by using other algorithms like neural-nets, random-forest, etc..
the main goal when you deal with such an unbalanced data is to find the ideal way to balance the results and make it robust against pitfalls.


Information on the files:

1) training file is 'ISISORNOT_PIPE.py' - it includes PCA analysis

2) evaluation file is 'EVAL_ISISORNOT.py' - it loads the pipeline and the classifier and then classify

3) plotting the confusion matrix with the file 'PlotMat.py'

4) png files - 'ISISORNOT_NO_WEIGHTS', 'ISISORNOT', 'PCA_ANAL'.
