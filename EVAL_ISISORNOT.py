import numpy as np
import nltk
from nltk.stem import WordNetLemmatizer
nltk.download('stopwords')
nltk.download('wordnet')
import joblib
import re
import pandas as pd




path = r'E:\D\NLP'
# download data, removing problematic lines
DF_ISIS = pd.read_excel('tweets_isis_all.xlsx', engine='openpyxl') # path+'\\'+  , error_bad_lines=False
DF_Rand = pd.read_excel('tweets_random_all1.xlsx', engine='openpyxl') # path+'\\'+  , error_bad_lines=False

DF_All = pd.DataFrame({'tweets':[],'label':[]})
DF_All = DF_All.append(pd.DataFrame({'tweets':DF_ISIS['tweets'],'label':np.ones((len(DF_ISIS)))}))
DF_All = DF_All.append(pd.DataFrame({'tweets':DF_Rand['content'],'label':np.zeros((len(DF_Rand)))}))

documents = []

stemmer = WordNetLemmatizer()

for sen in range(0, len(DF_All['tweets'])):
    # Remove all the special characters
    document = re.sub(r'\W', ' ', str(DF_All['tweets'].iloc[sen]))

    # remove all single characters
    document = re.sub(r'\s+[a-zA-Z]\s+', ' ', document)

    # Remove single characters from the start
    document = re.sub(r'\^[a-zA-Z]\s+', ' ', document)

    # Substituting multiple spaces with single space
    document = re.sub(r'\s+', ' ', document, flags=re.I)

    # Removing prefixed 'b'
    document = re.sub(r'^b\s+', '', document)

    # Converting to Lowercase
    document = document.lower()

    # Lemmatization
    document = document.split()

    document = [stemmer.lemmatize(word) for word in document]
    document = ' '.join(document)

    documents.append(document)



tfidf = joblib.load('tfidf.joblib')

X = tfidf.transform(documents).toarray()

clf = joblib.load('Classifier.joblib')
Pred = clf.predict(X)
pd.DataFrame({'Pred':Pred}).to_csv('df_out.csv')