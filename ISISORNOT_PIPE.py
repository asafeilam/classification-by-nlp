
import os
import numpy as np
import nltk
from nltk.stem import WordNetLemmatizer
nltk.download('stopwords')
nltk.download('wordnet')
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix
from sklearn.metrics import plot_confusion_matrix
from sklearn.pipeline import Pipeline
import joblib
import pickle
from nltk.corpus import stopwords
import re
import pandas as pd
import matplotlib.pyplot as plt
from PlotMat import plot_confusion_matrix
import seaborn
from sklearn.decomposition import PCA

path = r'E:\D\NLP'
# download data, removing problematic lines
DF_ISIS = pd.read_excel(path+'\\'+'tweets_isis_all.xlsx', engine='openpyxl')
DF_Rand = pd.read_excel(path+'\\'+'tweets_random_all.xlsx', engine='openpyxl', error_bad_lines=False)

DF_All = pd.DataFrame({'tweets':[],'label':[]})
DF_All = DF_All.append(pd.DataFrame({'tweets':DF_ISIS['tweets'],'label':np.ones((len(DF_ISIS)))}))
DF_All = DF_All.append(pd.DataFrame({'tweets':DF_Rand['content'],'label':np.zeros((len(DF_Rand)))}))

documents = []

stemmer = WordNetLemmatizer()

for sen in range(0, len(DF_All['tweets'])):
    # Remove all the special characters
    document = re.sub(r'\W', ' ', str(DF_All['tweets'].iloc[sen]))

    # remove all single characters
    document = re.sub(r'\s+[a-zA-Z]\s+', ' ', document)

    # Remove single characters from the start
    document = re.sub(r'\^[a-zA-Z]\s+', ' ', document)

    # Substituting multiple spaces with single space
    document = re.sub(r'\s+', ' ', document, flags=re.I)

    # Removing prefixed 'b'
    document = re.sub(r'^b\s+', '', document)

    # Converting to Lowercase
    document = document.lower()

    # Lemmatization
    document = document.split()

    document = [stemmer.lemmatize(word) for word in document]
    document = ' '.join(document)

    documents.append(document)


vectorizer = CountVectorizer(max_features=1500, min_df=5, max_df=0.7, stop_words=stopwords.words('english'))
#X = vectorizer.fit_transform(documents).toarray()

# Number of Occurrences of a word/Total words in the document
#Word_frequency = X/np.outer(np.sum(X,axis=1),np.ones((1500,1)))

tfidfconverter = TfidfTransformer()
#X = tfidfconverter.fit_transform(X).toarray()
clf = LogisticRegression(random_state=0,class_weight={ 0:0.1, 1:0.9 })
#clf = LogisticRegression(random_state=0)
pipe = Pipeline([('vectorizer',vectorizer),('tfidf',tfidfconverter)])
pipe.fit_transform(documents)
X = pipe.transform(documents).toarray()
y = DF_All['label']
LconfMat = []

############## pca analysis ##############
pca = PCA(n_components=2)
pca.fit(X)
PX = pca.transform(X)
plt.figure()
plt.plot(PX[:,0],PX[:,1],'o',label='All Data')
plt.plot(PX[:len(DF_ISIS),0],PX[:len(DF_ISIS),1],'ro',label='ISIS')
plt.xlabel('1st component')
plt.ylabel('2nd component')
plt.title('PCA Scatter')
plt.legend(loc='lower right')
##############################################
# running the classifier with different datasets each iteration with random train and test
joblib.dump(pipe,'tfidf.joblib')
for K0 in range(0,10):

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)  # , random_state=42

    clf = clf.fit(X_train, y_train)  # LogisticRegression(random_state=0,class_weight={ 0:0.1, 1:0.9 })
    clf.score(X_train, y_train)
    clf.score(X_test, y_test)
    print(K0)


    predtest = clf.predict(X_test)
    ConMat = confusion_matrix(y_test, predtest)
    A = np.zeros((2,2))
    A[0,0] = ConMat[0,0]/ConMat.sum(axis=1)[0]
    A[0,1] = ConMat[0,1]/ConMat.sum(axis=1)[0]
    A[1,0] = ConMat[1,0]/ConMat.sum(axis=1)[1]
    A[1,1] = ConMat[1,1]/ConMat.sum(axis=1)[1]
    LconfMat.append(A)
    del X_train, X_test, y_train, y_test

Acumsum = np.zeros((2,2))
ArrStat = np.zeros((10,4))
for K1 in range (0,10):
    Acumsum = Acumsum+LconfMat[K1]
    ArrStat[K1,0] = LconfMat[K1][0,0]
    ArrStat[K1,1] = LconfMat[K1][0,1]
    ArrStat[K1,2] = LconfMat[K1][1,0]
    ArrStat[K1,3] = LconfMat[K1][1,1]

ArrStat.mean(axis=0)
MeanConfMat = np.zeros((2,2))
MeanConfMat[0,0] = ArrStat[0,0]
MeanConfMat[0,1] = ArrStat[0,1]
MeanConfMat[1,0] = ArrStat[0,2]
MeanConfMat[1,1] = ArrStat[0,3]


ArrStat.std(axis=0)
joblib.dump(clf, 'Classifier.joblib')
#plot_confusion_matrix(clf, X_test, y_test)
#plt.show()

#tfidf = joblib.load('tfidf.joblib')

#tfidf.transform(documents)

classes = ['NONTERROR', 'TERROR']
output_filename = 'ISISORNOT_NO_WEIGHTS'
plot_confusion_matrix(MeanConfMat, classes, output_filename)
########################################################################
