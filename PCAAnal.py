
import numpy as np
import nltk
from nltk.stem import WordNetLemmatizer
nltk.download('stopwords')
nltk.download('wordnet')
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from nltk.corpus import stopwords
import re
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA

path = r'E:\D\NLP'
# download data, removing problematic lines
DF_ISIS = pd.read_excel('tweets_isis_all.xlsx', engine='openpyxl')
DF_Rand = pd.read_excel('tweets_random_all1.xlsx', engine='openpyxl')

DF_All = pd.DataFrame({'tweets':[],'label':[]})
DF_All = DF_All.append(pd.DataFrame({'tweets':DF_ISIS['tweets'],'label':np.ones((len(DF_ISIS)))}))
DF_All = DF_All.append(pd.DataFrame({'tweets':DF_Rand['content'],'label':np.zeros((len(DF_Rand)))}))

documents = []

stemmer = WordNetLemmatizer()

for sen in range(0, len(DF_All['tweets'])):
    # Remove all the special characters
    document = re.sub(r'\W', ' ', str(DF_All['tweets'].iloc[sen]))

    # remove all single characters
    document = re.sub(r'\s+[a-zA-Z]\s+', ' ', document)

    # Remove single characters from the start
    document = re.sub(r'\^[a-zA-Z]\s+', ' ', document)

    # Substituting multiple spaces with single space
    document = re.sub(r'\s+', ' ', document, flags=re.I)

    # Removing prefixed 'b'
    document = re.sub(r'^b\s+', '', document)

    # Converting to Lowercase
    document = document.lower()

    # Lemmatization
    document = document.split()

    document = [stemmer.lemmatize(word) for word in document]
    document = ' '.join(document)

    documents.append(document)


vectorizer = CountVectorizer(max_features=1500, min_df=5, max_df=0.7, stop_words=stopwords.words('english'))
#X = vectorizer.fit_transform(documents).toarray()

# Number of Occurrences of a word/Total words in the document
#Word_frequency = X/np.outer(np.sum(X,axis=1),np.ones((1500,1)))

tfidfconverter = TfidfTransformer()
#X = tfidfconverter.fit_transform(X).toarray()
clf = LogisticRegression(random_state=0,class_weight={ 0:0.1, 1:0.9 })
#clf = LogisticRegression(random_state=0)
pipe = Pipeline([('vectorizer',vectorizer),('tfidf',tfidfconverter)])
pipe.fit_transform(documents)
X = pipe.transform(documents).toarray()
y = DF_All['label']
LconfMat = []

############## pca analysis ##############
pca = PCA(n_components=2)
pca.fit(X)
PX = pca.transform(X)
plt.figure()
plt.plot(PX[:,0],PX[:,1],'o',label='All Data')
plt.plot(PX[:len(DF_ISIS),0],PX[:len(DF_ISIS),1],'ro',label='ISIS')
plt.xlabel('1st component')
plt.ylabel('2nd component')
plt.title('PCA Scatter')
plt.legend(loc='lower right')
plt.savefig('pca.png')
